<?php

namespace Database\Factories;

use App\Models\LogEntry;
use Illuminate\Database\Eloquent\Factories\Factory;

class LogEntryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LogEntry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $levels = [
            'emergency',
            'alert',
            'critical',
            'error',
            'warning',
            'notice',
            'info',
            'debug',
        ];
        return [
            'domain' => $this->faker->domainName(),
            'level' => $this->faker->randomElement($levels),
            'message' => $this->faker->sentence(),
            'stacktrace' => $this->faker->paragraph(),
            'context' => [
                'var1' => 'value1',
                'var2' => 'value2',
                'var3' => 'value3',
            ],
        ];
    }
}
