<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LogEntryController;
use App\Http\Controllers\PushSubscriptionController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'), 302);
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [LogEntryController::class, 'index'])->name('dashboard');
    // Route::get('/data', [LogEntryController::class, 'entries'])->name('xhr.entries');
    Route::get('/purge', function () {
        \App\Actions\PurgeRecords::perform();
        return redirect('/dashboard')->with('success', 'All entries purged');
    })->name('purge');

    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');

    // Push Subscriptions
    Route::post('subscriptions', [PushSubscriptionController::class, 'update']);
    Route::post('subscriptions/delete', [PushSubscriptionController::class, 'destroy']);
});

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');


Route::get('manifest.json', function () {
    
    return [
        "icons" => [
            [
                "src" => "/images/logo.192.png",
                "type" => "image/png",
                "sizes" => "192x192",
            ]
        ],
        'name' => config('app.name'),
        'short_name' => 'LogMonster',
        'start_url' => "/dashboard",
        'display' => "standalone",
        'scope' => "/",
        'theme_color' => "#505050",
        'background_color' => "#fff"
    ];
});
