<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LogEntryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('log/store', [LogEntryController::class, 'store'])
    ->middleware('auth.ip')
    ->name('log.store');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
