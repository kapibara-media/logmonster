<div class="pl-4 mb-2 text-lg {{ $entry->color }}">
    <div class="bg-white p-4">
        <div class="text-xl flex justify-between">
            <span>{{ $entry->created_at->format('d-m H:i:s') }}</span>
            <span>{{ $entry->level_name }}</span>
        </div>
        <div><a class="underline" href="{{ $entry->domain }}">{{ $entry->domain }}</a></div>
        <div>{{ $entry->message }}</div>
    </div>
</div>
