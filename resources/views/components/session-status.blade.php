@props(['status'])

@if ($status)
    <div {{ $attributes->merge(['class' => 'font-medium my-4 py-2 rounded text-lg text-center']) }}>
        {{ $status }}
    </div>
@endif
