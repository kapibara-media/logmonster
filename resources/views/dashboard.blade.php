<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Log entries') }} {{ now()->format('d-m H:i') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="shadow-sm sm:rounded-lg">
                <button class="w-full my-2 py-4 text-2xl text-center text-white bg-blue-500 rounded"
                    onclick="window.location.reload();">What&rsquo;s up, pussycat?</button>
                @if ($entries->count() === 0)
                    <p class="py-4 text-2xl text-center bg-white">Don&lsquo;t worry, be happy.</p>
                @else
                    <div id="entries">
                        @foreach ($entries as $entry)
                            <x-log-entry :entry="$entry"></x-log-entry>
                        @endforeach
                    </div>
                    {{ $entries->links() }}
                @endif
                <x-subscribe-push></x-subscribe-push>
            </div>
        </div>
    </div>
</x-app-layout>
