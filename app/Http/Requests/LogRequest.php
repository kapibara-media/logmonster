<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'domain' => 'string|required',
            'level' => 'integer|required',
            'level_name' => 'string|required',
            'message' => 'string|required',
            'extra' => 'array',
            'context' => 'array',
        ];
    }
}
