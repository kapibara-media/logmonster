<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\LogEntry;
use App\Http\Requests\LogRequest;
use Illuminate\Support\Facades\Log;
use App\Notifications\NewLogRecordNotification;

class LogEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = LogEntry::select(['id', 'domain', 'level', 'level_name', 'message', 'created_at'])->latest()->paginate(10);
        return view('dashboard', compact('entries'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function entries()
    {
        return LogEntry::select(['id', 'domain', 'level', 'level_name', 'message', 'created_at'])->latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LogRequest $request)
    {
        if ($request->level >= config('logging.treshold')) {
            try {
                User::first()->notify(
                    new NewLogRecordNotification(
                        LogEntry::create($request->validated())
                    )
                );
            } catch(\Exception $e) {
                Log::critical($e);
                Log::error($request->message, $request->context ?? []);
                return response('NOK', 500);
            }
        }
        return response('OK', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LogEntry  $logEntry
     * @return \Illuminate\Http\Response
     */
    public function show(LogEntry $logEntry)
    {
        return response()->json($logEntry);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LogEntry  $logEntry
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogEntry $logEntry)
    {
        $logEntry->delete();
        return response()->json(['result' => 'OK']);
    }
}
