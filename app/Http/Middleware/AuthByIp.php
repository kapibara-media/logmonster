<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthByIp
{
    protected function allowed()
    {
        return [
            '127.0.0.1',
            '136.144.130.108', // martijnvreeken-vps
            '2a01:7c8:fff9:66:5054:ff:fe40:bd23', // martijnvreeken-vps
            '136.144.234.22', // mijn.zijderlaan.nl
            '2a01:7c8:d005:17b:5054:ff:fe46:553b', // mijn.zijderlaan.nl
            '136.144.246.236', // de-spil-trainingen
            '2a01:7c8:d006:313:5054:ff:feb8:6388', // de-spil-trainingen
            '136.144.252.165', // Collectief Belang
            '2a01:7c8:d007:3eb:5054:ff:fe2b:c039', // Collectief Belang
            '136.144.244.70', // Mainplus
            '2a01:7c8:d006:101:5054:ff:fec1:7acb', // Mainplus
            '136.144.252.165', // energie-crm.nl
            '2a01:7c8:d007:3eb:5054:ff:fe2b:c039', // energie-crm.nl
        ];
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!\in_array($request->ip(), $this->allowed())) {
            abort(403);
        }
        return $next($request);
    }
}
