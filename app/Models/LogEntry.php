<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogEntry extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $casts = [
        'context' => 'array',
    ];

    public function getColorAttribute()
    {
        switch($this->level) {
            case 600:
                return 'bg-red-600';
            case 550:
                return 'bg-yellow-500';
            case 500:
                return 'bg-red-400';
            case 400:
                return 'bg-red-200';
            case 300:
                return 'bg-yellow-300';
            case 250:
                return 'bg-blue-400';
            case 200:
                return 'bg-blue-200';
            case 100:
            default:
                return 'bg-gray-300';
        }
    }
}
