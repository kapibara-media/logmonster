<?php
namespace App\Actions;

use App\Helpers\Url;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Composer\CaBundle\CaBundle;
use GuzzleHttp\Client as HttpClient;

class PingWebsite
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new HttpClient([
            'timeout' => 20,
            RequestOptions::VERIFY => CaBundle::getBundledCaBundlePath(),
        ]);
    }

    public function perform(string $url): bool
    {
        $request = new Request('GET', Url::addProtocol($url));

        try {
            $response = $this->client->send($request);
        } catch (\Exception $e) {
            return false;
        }
        return $response->getStatusCode() < 400;
    }
}