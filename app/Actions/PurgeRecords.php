<?php
namespace App\Actions;

use App\Models\LogEntry;

abstract class PurgeRecords
{
    public static function perform()
    {
        LogEntry::where('created_at', '<', now()->subMonth(1))->delete();
    }
}