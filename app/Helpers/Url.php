<?php
namespace App\Helpers;

use Illuminate\Support\Str;

abstract class Url
{
    public static function addProtocol(string $url): string
    {
        if (! Str::startsWith($url, 'http')) {
            $url = 'https://' . $url;
        }
        return $url;
    }
}