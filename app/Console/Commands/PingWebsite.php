<?php

namespace App\Console\Commands;

use App\Helpers\Url;
use App\Models\User;
use App\Models\LogEntry;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\NewLogRecordNotification;

class PingWebsite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:ping {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check whether a website is online.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = $this->argument('url');
        $action = new \App\Actions\PingWebsite;
        if ($action->perform($url)) {
            $this->info('Website online');
            return 1;
        }
        $this->error('Website is offline');
        try {
            User::first()->notify(
                new NewLogRecordNotification(
                    LogEntry::create([
                        'domain' => Url::addProtocol($url),
                        'level' => 600,
                        'level_name' => 'CRITICAL',
                        'message' => 'Offline',
                    ])
                )
            );
        } catch(\Exception $e) {
            Log::critical($e);
        }
        return 0;
    }
}
