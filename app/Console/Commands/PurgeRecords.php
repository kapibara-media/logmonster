<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PurgeRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'entries:purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge all log entries from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \App\Actions\PurgeRecords::perform();
        $this->info('All entries purged');
        return 1;
    }
}
