<?php

namespace App\Notifications;

use App\Models\LogEntry;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;
use Illuminate\Notifications\Messages\MailMessage;

class NewLogRecordNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $record;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(LogEntry $record)
    {
        $this->record = $record;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('LogMonster Event')
            ->line(__(':domain encountered a :level error.', ['domain' => $this->record->domain, 'level' => $this->record->level_name]))
            ->action('Go to logmonster', url('/dashboard'))
            ->line($this->record->message)
            ->salutation('Have a monstrous good day,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'domain' => $this->record->domain,
            'level' => $this->record->level_name,
            'message' => $this->record->message
        ];
    }

    /**
     * Get the web push representation of the notification.
     *
     * @param  mixed  $notifiable
     * @param  mixed  $notification
     * @return \Illuminate\Notifications\Messages\DatabaseMessage
     */
    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title('Log Monster event')
            ->icon('/notification-icon.png')
            ->body(__(':level on :domain', ['level' => $this->record->level_name, 'domain' => $this->record->domain]))
            ->action('View log', 'view_log')
            ->data(['id' => $notification->id]);
    }
}
