<?php

return [
    'ping' => [
        'websites' => ENV('PING_URLS'),
    ],
];
